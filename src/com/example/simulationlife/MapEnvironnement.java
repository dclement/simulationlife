package com.example.simulationlife;

import java.util.HashMap;

/**
 * Classe contenant les informations de bases sur les positions et tailles des images initialement cr��es
 * @author Clement
 *
 */
public class MapEnvironnement {

	private int width;
	private int height;
	private HashMap<Integer, PositionMapObject> hashInitial;
	
	public MapEnvironnement(int widthEnv, int heightEnv, HashMap<Integer, PositionMapObject> hash)
	{
		this.width = widthEnv;
		this.height = heightEnv;
		hashInitial = hash;
	}
	
	public int getWidth()
	{
		return width;
	}
	public int getHeight()
	{
		return height;
	}
	public HashMap<Integer, PositionMapObject> getHash()
	{
		return hashInitial;
	}
}
