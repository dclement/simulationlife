package com.example.simulationlife;

/**
 * Classe regroupant les informations de dimension et position des images affich�s et sa correspondance � la question pos�e
 * @author Clement
 *
 */
public class PositionMapObject {
	// Coordonn�es de l'image
	private int positionX;
	private int positionY;
	// Dimension de l'image
	private int width;
	private int height;
	// Numero de la question correspondance
	private int idQuestion;
	
	public PositionMapObject(int x, int y, int width, int height, int id)
	{
		positionX = x;
		positionY = y;
		this.width = width;
		this.height = height;
		idQuestion = id;
	}
	
	public int getPositionX()
	{
		return positionX;
	}
	public int getPositionY()
	{
		return positionY;
	}
	public int getWidth()
	{
		return width;
	}
	public int getHeight()
	{
		return height;
	}
	public int getIdQuestion()
	{
		return idQuestion;
	}
}
