package com.example.simulationlife;

import java.util.ArrayList;

/**
 * Classe regroupant les attributs se trouvant dans le xml
 * @author Clement
 *
 */
public class Question {
			 
	// Titre de la dialoguebox
	public String Title;
	// Question pos�e
	public String Text;
	// Type de la reponse attendu text ou nombre
	public String format;
	// Liste des choix possibles
	public ArrayList<String> ListChoix = new ArrayList<String>(3);
	// Deuxi�me question a poser pour la m�me interaction
	public int questionSuite;
	// Verification de l'etat de la question, repondu ou non
	private boolean Repondu = false;
	// Enregistrement de la reponse de l'utilisateur
	private String Reponse="";
	
	/**
	 * Constructeur pour les questions avec choix multipes
	 * @param title
	 * @param text
	 * @param Choix1
	 * @param Choix2
	 * @param Choix3
	 */
	public Question(String title, String text, String Choix1, String Choix2, String Choix3, int questionSuivant)
	{
		Title = title;
		Text = text;
		ListChoix.add(Choix1);
		ListChoix.add(Choix2);
		ListChoix.add(Choix3);
		format = "indef";
		questionSuite = questionSuivant;
	}
	
	/**
	 * Constructeur pour les autres question
	 * @param title
	 * @param text
	 * @param format nombre ou text defini le type de reponse attendu
	 */
	public Question(String title, String text, String format, int questionSuivant)
	{
		this.Title = title;
		this.Text = text;
		this.format = format; 
		questionSuite = questionSuivant;
	}
	
	public void setRepondu(boolean rep)
	{
		Repondu = rep;
	}
	
	public boolean getRepondu()
	{
		return Repondu;
	}
	
	public void setReponse(String rep)
	{
		Reponse = rep;
	}
	
	public String getReponse()
	{
		return Reponse;
	}
	
}
