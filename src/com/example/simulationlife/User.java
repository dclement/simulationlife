package com.example.simulationlife;

/**
 * Classe regroupant les informations relatifs au joueur
 * @author Clement
 *
 */
public class User {
	/* Utilisation du dessign patern Singleton */
	public String name;
	public int positionX = 0 ;
	public int positionY = 0 ;
	// 1 bas, 2 haut, 3 gauche, 4 droite
	public int orientation = 2;
	
	
	
	/** Constructeur priv� */
	private User()
	{}
	 
	/** Instance unique non pr�initialis�e */
	private static User INSTANCE = null;
	 
	/** Point d'acc�s pour l'instance unique du singleton */
	public static User getInstance()
	{	
		if (INSTANCE == null)
		{ 
			INSTANCE = new User();	
		}
		return INSTANCE;
	}
}
