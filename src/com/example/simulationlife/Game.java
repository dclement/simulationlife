package com.example.simulationlife;

import java.util.HashMap;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.View;

/**
 * Classe dessinant l'environnement
 * @author Clement
 *
 */
public class Game extends View {
	// Variable de chargement des bitmaps
	Bitmap mBitmap;
	// Recuperation des positions de l'utilisateur
	User UserPosition;
	// Dimensions de l'ecran de l'utilisateur
	int deviceWidth;
	int deviceHeight;
	// Tab des bitmaps
	public HashMap<Integer, Bitmap> HashBitmap;
	// Tab des PositionMapObjects
	public HashMap<Integer,PositionMapObject> HashPosition;
	// Dimensions de l'image mapmaison dans les ressources
	public int dimMapWidth = 526;
	public int dimMapHeight = 259;
	// Pas de deplacement du joueur
	private int deplacementUser;
	
	public Game(Activity _activity, User currentUser, int deviceWidth, int deviceHeight, MapEnvironnement env) {
	        super(_activity);
	        this.deviceWidth = deviceWidth;
	        this.deviceHeight = deviceHeight;
	        this.dimMapWidth = env.getWidth();
	        this.dimMapHeight = env.getHeight();
	        HashBitmap = new HashMap<Integer, Bitmap>(5);
	        HashBitmap.put(1, BitmapFactory.decodeResource(getResources(), R.drawable.front));
	        HashBitmap.put(2, BitmapFactory.decodeResource(getResources(), R.drawable.back));
	        HashBitmap.put(3, BitmapFactory.decodeResource(getResources(), R.drawable.left));
	        HashBitmap.put(4, BitmapFactory.decodeResource(getResources(), R.drawable.right));
	        HashBitmap.put(5, BitmapFactory.decodeResource(getResources(), R.drawable.mapmaison));
	        
	        UserPosition = currentUser;
	        
	        HashPosition = new HashMap<Integer, PositionMapObject>(env.getHash().size());
	        for(int i=1;i<env.getHash().size()+1;i++)
	        {
	        	Point pt = positionInteractionCalcul(env.getHash().get(i).getPositionX(),env.getHash().get(i).getPositionY(),deviceWidth,deviceHeight);
	  	        Point pt2 = positionInteractionCalcul(env.getHash().get(i).getWidth(),env.getHash().get(i).getHeight(),deviceWidth,deviceHeight);
	  	        HashPosition.put(i, new PositionMapObject(pt.x,pt.y, pt2.x, pt2.y, env.getHash().get(i).getIdQuestion()));
	        }
	    }
	
	
	/**
	 * Calcul de la position de l'image en fonction de la taille de l'ecran
	 * @param x position de l'image sur la carte initiale // dimension de l'image sur la carte initiale 
	 * @param y position de l'image sur la carte initiale // dimension de l'image sur la carte initiale 
	 * @param widthMap taille de l'ecran de l'utilisateur
	 * @param heightMap taille de l'ecran de l'utilisateur
	 * @return Cordonnées x et y de l'image / dimension 
	 */
	public Point positionInteractionCalcul(int x,int y,int widthMap, int heightMap)
	{
		float facteurHeight = (float)heightMap/dimMapHeight;
		float facteurWidth = (float)widthMap/dimMapWidth; 
		return new Point((int)Math.round(x*facteurWidth),(int)Math.round(y*facteurHeight));
	}
	
	/**
	 * Fonction retourne le pas de deplacement du joueur
	 * @return le pas de deplacement du joueur
	 */
	public int getWidthBitmapPlayer()
	{
		return deplacementUser;
	}
	
	
	/**
	 * Fonction de restriction des deplacements de l'utilisateur
	 * @param emplacement vers quel endroit l'utilisateur se deplace
	 * @return la limite de la carte 
	 */
	public int limiteMap(int emplacement)
	{
		int limite = 0;
		switch(emplacement)
		{
			//bas et haut
			case 1: limite = deviceHeight/2;
				break;
			//droite et gauche
			case 2: limite = deviceWidth/2;
				break;
		}
		return limite;
		
	}
	
	
	
	 @Override
	    protected void onDraw(Canvas can) {

		 	can.drawColor(Color.BLACK);
		 	//draw maison
		    Bitmap resizedBitmapMap = Bitmap.createScaledBitmap( HashBitmap.get(5), deviceWidth, deviceHeight, false);
		 	can.drawBitmap(resizedBitmapMap, -UserPosition.positionX, -UserPosition.positionY, new Paint());
		 	
		 	//Affichage de la bonne image suivant l'orientation du personnage
	        switch(UserPosition.orientation)
	        {
		        case 1: mBitmap = HashBitmap.get(1);
		        	break;
		        case 2: mBitmap = HashBitmap.get(2);
		        	break;
		        case 3: mBitmap = HashBitmap.get(3);
		        	break;
		        case 4: mBitmap = HashBitmap.get(4);
		        	break;
	        }
	        Bitmap resizedBitmap = Bitmap.createScaledBitmap(mBitmap, deviceHeight/10, deviceHeight/10, false);
	        //Deplacement du personnage = la moitie de la taille de l'image du personnage
	        deplacementUser = resizedBitmap.getWidth()/2;
	        // Draw User au milieu de l'ecran
	        can.drawBitmap(resizedBitmap, (deviceWidth/2)- resizedBitmap.getWidth()/2 , (deviceHeight/2) - resizedBitmap.getHeight()/2, new Paint());
	       
	    	
	    	
	 }

	 /**
	  * Function de calcul des interactions entre l'utilisateur et les images pour ensuite lui poser les questions
	  * @return id de la question sinon -1
	  */
	public int interactionPossible() {
		int idQuestion = -1;
		for(int i =1;i<HashPosition.size()+1;i++)
		{
			if((UserPosition.positionX+deviceWidth/2)>HashPosition.get(i).getPositionX() && (UserPosition.positionX+deviceWidth/2)<(HashPosition.get(i).getPositionX()+HashPosition.get(i).getWidth()))
			{
				if((UserPosition.positionY+deviceHeight/2)>HashPosition.get(i).getPositionY() && (UserPosition.positionY+deviceHeight/2)<(HashPosition.get(i).getPositionY()+HashPosition.get(i).getHeight()))
				{
					idQuestion = HashPosition.get(i).getIdQuestion();
				}
			}
		}
		return idQuestion;
	}
}
