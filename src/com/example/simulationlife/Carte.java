package com.example.simulationlife;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.xmlpull.v1.XmlSerializer;

import java.util.Arrays;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


/**
 * Activity principal du jeu
 * @author Clement
 *
 */
@SuppressLint("NewApi")
public class Carte extends Activity {

	public User CurrentUser = null;
	public int dimensionImageUser; 
	public Game viewGame;
	public XMLParser XMLParserClass = null;
	public MapEnvironnement Env = null;
	public String filename = "myfile";
	public String playerName;
	public boolean dialog = false;
	public Carte()
	{
		//Creation du joueur
		CurrentUser = User.getInstance();
		//Reset position du joueur
		CurrentUser.positionX = 0;
		CurrentUser.positionY = 0;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game, menu);
		return true;
	}
	
	
	// Action sur la bar de menu
	public boolean onOptionsItemSelected(MenuItem item) {	
        switch (item.getItemId()) {
          // retour au menu principal si reponse = oui 
          case R.id.Retour:
	        	AlertDialog.Builder diagQuestion;
	      		diagQuestion = new AlertDialog.Builder(this);
	      		diagQuestion.setTitle("Retour au menu principal");
	      		diagQuestion.setMessage("Etes vous s�r de revenir au menu principal ? Votre progression ne sera pas sauvegard�e."); 
	      		diagQuestion.setCancelable(false);	//oblige le joueur � repondre � la question pour quitter la fenetre 
	      		
	      		diagQuestion.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
	                  
	                  public void onClick(DialogInterface dialog, int which) {
	                  		finish(); // fermeture de l'activit� courante
	    	        	  	Intent myIntent = new Intent(getBaseContext(), MainActivity.class); //Initialisation de la prochaine activit�    
	    	        	  	startActivity(myIntent);
	                  }});
	              	              
	      		diagQuestion.setNegativeButton("Non", new DialogInterface.OnClickListener() {
	                 
	                  public void onClick(DialogInterface dialog, int which) {
	                  }});
	              
	      		diagQuestion.show();
               break;
          case R.id.Resultat:
        	  if(XMLParserClass!=null)
        	  {
           		  setContentView(R.layout.resultat);
           		  TextView text = (TextView) findViewById(R.id.stringResultat);
           		  text.setText(readtxt(filename));
           		  TextView nameplayer = (TextView) findViewById(R.id.namePLayer);
         		  nameplayer.setText(playerName);
        	  }
          	break;
        }
        return false;
    }
	

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.frame);
		// Clear les reponses precedentes
		clearTxtFile(getApplicationContext().getFilesDir(),filename);
		Intent intent = getIntent();
		playerName = intent.getExtras().getString("NamePlayer");
		
		// ouverture du fichier XML
		InputStream in_s = null;
		try {
			in_s = getApplicationContext().getAssets().open("Question.xml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		// Parse Question.xml dans la classe XMLParser
		XMLParserClass = new XMLParser();
		XMLParserClass.readXml(in_s);
		
		
		// ouverture du fichier XML
		InputStream in_s2 = null;
		try {
			in_s2 = getApplicationContext().getAssets().open("Positioninteraction.xml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Creation de la classe mapEnvironnement regroupant les informations a afficher dans game
		Env = XMLParserClass.readPositionEnvironnement(in_s2);
			
		/* 
		 * Information sur la dimension de l'ecran de l'utilisateur pour le scale des images
		 */
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;
		
		
		// Ajout dans le layout de la carte ou le joueur va evoluer
		RelativeLayout layout = (RelativeLayout) findViewById(R.id.Layout);
		viewGame = new Game(this,CurrentUser,width,height,Env);
		layout.addView(viewGame);
	
		// Ajout dans le layoutButton (situ� au dessus du layout precedent) de 4 boutons permettant a l'utilisateur de se deplacer
		RelativeLayout layoutButton = (RelativeLayout) findViewById(R.id.LayoutButton);
		final Button tv1 = new Button(this);
		final Button tv2 = new Button(this);
		final Button tv3 = new Button(this);
		final Button tv4 = new Button(this);
			   
		//1st button
		RelativeLayout.LayoutParams lprams = new RelativeLayout.LayoutParams(
	            RelativeLayout.LayoutParams.WRAP_CONTENT,
	            RelativeLayout.LayoutParams.WRAP_CONTENT);
		lprams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
	    tv1.setText("Bottom");
	    tv1.setLayoutParams(lprams);
	    tv1.setId(1);
	    layoutButton.addView(tv1);	    
	    tv1.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	// Si l'utilisateur depasse la limite de la carte --> desactivation du bouton
	        	if(CurrentUser.positionY + viewGame.getWidthBitmapPlayer()>viewGame.limiteMap(1))
	        	{
	        		tv1.setEnabled(false);
	        	}
	        	else
	        	{
	        		// MAJ de la position et de l'orientation du joueur
		            CurrentUser.positionY = CurrentUser.positionY + viewGame.getWidthBitmapPlayer();
		            CurrentUser.orientation = 1;
		            // Repaint du canvas et pose eventuelle d'une question
		            viewGame.invalidate();
		            PoseQuestion();
	        	}
	        	if(!tv3.isEnabled())
	        	{
	        		tv3.setEnabled(true);
	        	}
	        }

			
	    });
	    
	    
	    //2st button
		RelativeLayout.LayoutParams lprams2 = new RelativeLayout.LayoutParams(
	            RelativeLayout.LayoutParams.WRAP_CONTENT,
	            RelativeLayout.LayoutParams.WRAP_CONTENT);
		lprams2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		lprams2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		    tv2.setText("Right");
	    tv2.setLayoutParams(lprams2);
	    tv2.setId(2);
	    layoutButton.addView(tv2);	    
	    tv2.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	// Si l'utilisateur depasse la limite de la carte --> desactivation du bouton
	        	if(CurrentUser.positionX + viewGame.getWidthBitmapPlayer()>viewGame.limiteMap(2))
	        	{
	        		tv2.setEnabled(false);
	        	}
	        	else
	        	{
	        		// MAJ de la position et de l'orientation du joueur
		            CurrentUser.positionX = CurrentUser.positionX + viewGame.getWidthBitmapPlayer();
		            CurrentUser.orientation = 4;
		            // Repaint du canvas et pose eventuelle d'une question
		            viewGame.invalidate();
		            PoseQuestion();
	        	}
	        	if(!tv4.isEnabled())
	        	{
	        		tv4.setEnabled(true);
	        	}
	        }
	    });
	    
	    //3 button
		RelativeLayout.LayoutParams lprams3 = new RelativeLayout.LayoutParams(
	            RelativeLayout.LayoutParams.WRAP_CONTENT,
	            RelativeLayout.LayoutParams.WRAP_CONTENT);
		lprams3.addRule(RelativeLayout.ABOVE,tv1.getId());
	     tv3.setText("Top");
	    tv3.setLayoutParams(lprams3);
	    tv3.setId(3);
	    layoutButton.addView(tv3);	    
	    tv3.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	// Si l'utilisateur depasse la limite de la carte --> desactivation du bouton
	        	if(CurrentUser.positionY - viewGame.getWidthBitmapPlayer()<-viewGame.limiteMap(1))
	        	{
	        		tv3.setEnabled(false);
	        	}
	        	else
	        	{
	        		// MAJ de la position et de l'orientation du joueur
		            CurrentUser.positionY = CurrentUser.positionY - viewGame.getWidthBitmapPlayer();
		            CurrentUser.orientation = 2;
		            // Repaint du canvas et pose eventuelle d'une question
		            viewGame.invalidate();
		            PoseQuestion();
	            }
	        	if(!tv1.isEnabled())
	        	{
	        		tv1.setEnabled(true);
	        	}
	        }
	    });
	    
	    
	    //4 button
	  		RelativeLayout.LayoutParams lprams4 = new RelativeLayout.LayoutParams(
	  	            RelativeLayout.LayoutParams.WRAP_CONTENT,
	  	            RelativeLayout.LayoutParams.WRAP_CONTENT);
	  		lprams4.addRule(RelativeLayout.ABOVE,tv2.getId());
	  		lprams4.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
	   	    tv4.setText("left");
	  	    tv4.setLayoutParams(lprams4);
	  	    tv4.setId(4);
	  	    layoutButton.addView(tv4);	    
	  	    tv4.setOnClickListener(new View.OnClickListener() {
	  	        @Override
	  	        public void onClick(View v) {
	  	        	// Si l'utilisateur depasse la limite de la carte --> desactivation du bouton
	  	        	if(CurrentUser.positionX - viewGame.getWidthBitmapPlayer()<-viewGame.limiteMap(2))
		        	{
		        		tv4.setEnabled(false);
		        	}
		        	else
		        	{
		        		// MAJ de la position et de l'orientation du joueur
		  	            CurrentUser.positionX= CurrentUser.positionX-viewGame.getWidthBitmapPlayer();
			            CurrentUser.orientation = 3;
			            // Repaint du canvas et pose eventuelle d'une question
			            viewGame.invalidate();
			            PoseQuestion();
		        	}
	  	        	if(!tv2.isEnabled())
		        	{
		        		tv2.setEnabled(true);
		        	}
	  	        }
	  	    });
	  	 
	}
	
	/**
	 * Fonction de clear du fichier des sauvegardes des resultats 
	 * @param filepath
	 * @param filename
	 */
public void clearTxtFile(File filepath, String filename)
{
	String string = "";
	FileOutputStream outputStream;
	try {
		  outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
		  outputStream.write(string.getBytes());
		  outputStream.close();
		} catch (Exception e) {
		  e.printStackTrace();
		}
	
}

/**
 * Ecriture des resultats dans le fichier de sauvegarde
 * @param filepath
 * @param filename nom du fichier
 */
public void writeintotxt(File filepath, String filename) {
	
		String string = "";
		String separateur = "\\";
		for(int i = 0; i<XMLParserClass.TabQuestion.size();i++)
		{
			string= string + XMLParserClass.TabQuestion.get(i).getReponse();
			string= string + separateur;
		}
		FileOutputStream outputStream;
		File file = new File(filepath, filename);

		try {
			  outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
			  outputStream.write(string.getBytes());
			  outputStream.close();
			} catch (Exception e) {
			  e.printStackTrace();
			}
	}

/**
 * 
 * @param filename non du fichier
 * @return le contenu du fichier en string
 */
public String readtxt(String filename)
{
	String retour = null;
	  try {
	        InputStream inputStream = openFileInput(filename);

	        if ( inputStream != null ) {
	            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String receiveString = "";
	            StringBuilder stringBuilder = new StringBuilder();

	            while ( (receiveString = bufferedReader.readLine()) != null ) {
	                stringBuilder.append(receiveString);
	            }

	            inputStream.close();
	            retour = stringBuilder.toString().replace("\\", " ");
	            System.out.println("fichier " + stringBuilder.toString());
	        }
	  }
	  catch (FileNotFoundException e) {
	        Log.e("login activity", "File not found: " + e.toString());
	    } catch (IOException e) {
	        Log.e("login activity", "Can not read file: " + e.toString());
	    }

	return retour;
}
	
	/**
	 * Recherche si une question doit etre pos�e
	 */
	public void PoseQuestion() {
		int id = viewGame.interactionPossible();
		// Une interaction entre le joueur et l'image retour de l'id de la question
		if(id!=-1)
		{
			Question Q = XMLParserClass.TabQuestion.get(id);
			// La question a t elle �t� deja repondu
			if(!Q.getRepondu())
			{
				AfficheQuestion(Q);
			}
		}
		
			
	}
	
	/**
	 * Fonction de sauvegarde des reponse dans un fichier externe
	 * Affichage d'une autre question li� a la precedente si 
	 * @param questionSuite different -1
	 */
	public void saveRep(int questionSuite)
	{
		// Sauvegarde resultat
		writeintotxt(getApplicationContext().getFilesDir(),filename);
		readtxt(filename);
		// Une autre question a pos� ?
		if(questionSuite!=-1)
		{
			Question Q = XMLParserClass.TabQuestion.get(questionSuite);
			if(!Q.getRepondu())
			{
				AfficheQuestion(Q);
			}
		}
	}
	
	/**
	 * Affichage de la question a pos�e
	 * @param Q classe question regroupant les attributs de la question 
	 * @return 
	 */
	private void AfficheQuestion(final Question Q) {
		
		AlertDialog.Builder diagQuestion;
  		diagQuestion = new AlertDialog.Builder(this);
  		
		LayoutInflater inflater = getLayoutInflater();
	
      	diagQuestion.setTitle(Q.Title);
      	diagQuestion.setMessage(Q.Text); 
      	diagQuestion.setCancelable(false);	//oblige le joueur � repondre � la question pour quitter la fenetre 
      		
      	// Question avec un choix
      	if(!Q.ListChoix.isEmpty())
      	{
      		diagQuestion.setPositiveButton(Q.ListChoix.get(0), new DialogInterface.OnClickListener() {
                  
                 public void onClick(DialogInterface dialog, int which) {
                	 Q.setReponse(Q.ListChoix.get(0));
                	 saveRep(Q.questionSuite);
                 }});
      		diagQuestion.setNeutralButton(Q.ListChoix.get(1), new DialogInterface.OnClickListener() {
                
      			 public void onClick(DialogInterface dialog, int which) {
      				 Q.setReponse(Q.ListChoix.get(1));
      				 saveRep(Q.questionSuite);
      			 }});
              	              
      		diagQuestion.setNegativeButton(Q.ListChoix.get(2), new DialogInterface.OnClickListener() {
                 
                 public void onClick(DialogInterface dialog, int which) {
                	 Q.setReponse(Q.ListChoix.get(2));
                	 saveRep(Q.questionSuite);
                 }});
      		Q.setRepondu(true);
      	}
      	else
      	{
      		// Question avec une reponse "ecrite" de l'utilisateur
      		final View Vtext = inflater.inflate(R.layout.dialoguetext, null);
      		final View Vnumber = inflater.inflate(R.layout.dialoguenumber, null);
      		
      		// Une reponse avec un chiffre
          	if(Q.format.equalsIgnoreCase("nombre"))
          	{
          		diagQuestion.setView(Vnumber);
          		diagQuestion.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    	EditText reponse = (EditText) Vnumber.findViewById(R.id.ReponseNumber);
                    	Q.setReponse(reponse.getText().toString());
                    	Q.setRepondu(true);
                    	saveRep(Q.questionSuite);
                    }});
        		diagQuestion.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    	Q.setRepondu(false);
                    }});
          	}// Une reponse en text
          	else if(Q.format.equalsIgnoreCase("text"))
          		 {
          			diagQuestion.setView(Vtext);
          			diagQuestion.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                        
                        public void onClick(DialogInterface dialog, int which) {
                        	
                        	EditText reponse = (EditText) Vtext.findViewById(R.id.Reponse);
                        	Q.setReponse(reponse.getText().toString());
                        	Q.setRepondu(true);
                        	saveRep(Q.questionSuite);
                        }});
            		diagQuestion.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                       
                        public void onClick(DialogInterface dialog, int which) {
                        	Q.setRepondu(false);
                        }});
          		 }
      			
      	}
      	diagQuestion.show();
	}

	}
