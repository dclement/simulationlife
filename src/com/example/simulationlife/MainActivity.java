package com.example.simulationlife;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


@SuppressLint("NewApi")
public class MainActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		Button myButton = (Button) findViewById(R.id.playButton);
		myButton.setOnClickListener(new View.OnClickListener() {
		   public void onClick(View v) {
		       EditText namePlayer = (EditText) findViewById(R.id.namePlayerText);
			   final String name2Player = namePlayer.getText().toString();
			  
			   if(!name2Player.equals(""))
			   {
				    finish(); // fermeture de l'activit� courante
	         		Intent myIntent = new Intent(getBaseContext(), Carte.class); //Initialisation de la prochaine activit�    
	         	    myIntent.putExtra("NamePlayer",name2Player);
				    startActivity(myIntent);
			   }
		   }
		});

	
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
		
	/**
	 * Function called when menu button is clicked
	 * @param item is the current button menu clicked
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		
        switch (item.getItemId()) {    	
          case R.id.Option:
        	  
               return true;
          case R.id.Quitter:
        	  finish();
              return true;
        }
        return false;
    }
	
}
