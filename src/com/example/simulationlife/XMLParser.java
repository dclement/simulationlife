package com.example.simulationlife;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;

/**
 * Classe servant a parser le XML Question.xml et creer les questions dans un hashtable
 * Classe servant a parser le XML PositionInteraction.xml et creer les PositionMapObject
 * @author Clement
 *
 */
public class XMLParser {
	// Hashtable regroupant les Questions
	public Hashtable<Integer, Question> TabQuestion;
	
	/**
	 * Constructeur
	 */
	public XMLParser()
	{
		TabQuestion = new Hashtable<Integer, Question>();
	}
	
	/**
	 * Fonction qui parse
	 * @param in_s document � parser
	 */
	public void readXml(InputStream in_s)
	{
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(in_s);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("Question");
			for (int temp = 0; temp < nList.getLength(); temp++) {
		 
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		 			Element eElement = (Element) nNode;
					TabQuestion.put(Integer.parseInt(eElement.getElementsByTagName("id").item(0).getTextContent()), new Question(eElement.getElementsByTagName("titre").item(0).getTextContent(),eElement.getElementsByTagName("text").item(0).getTextContent(),eElement.getElementsByTagName("format").item(0).getTextContent(),Integer.parseInt(eElement.getElementsByTagName("lie").item(0).getTextContent())));
				}
				
			}
			
			NodeList nListChoix = doc.getElementsByTagName("QuestionChoix");
			for (int tempChoix = 0; tempChoix < nListChoix.getLength(); tempChoix++) {
		 		Node nNodeChoix = nListChoix.item(tempChoix);
				if (nNodeChoix.getNodeType() == Node.ELEMENT_NODE) {
					Element eElementChoix = (Element) nNodeChoix;
					TabQuestion.put(Integer.parseInt(eElementChoix.getElementsByTagName("id").item(0).getTextContent()), new Question(eElementChoix.getElementsByTagName("titre").item(0).getTextContent(),eElementChoix.getElementsByTagName("text").item(0).getTextContent(),eElementChoix.getElementsByTagName("choix1").item(0).getTextContent(),eElementChoix.getElementsByTagName("choix2").item(0).getTextContent(),eElementChoix.getElementsByTagName("choix3").item(0).getTextContent(),Integer.parseInt(eElementChoix.getElementsByTagName("lie").item(0).getTextContent())));
				}
				
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		  }
	
	/**
	 * Fonction qui parse Positioninteraction
	 * @param in_s
	 * @return MapEnvironnement servant a game
	 */
	@SuppressLint("UseSparseArrays")
	public MapEnvironnement readPositionEnvironnement(InputStream in_s)
	{
		int widthEnv = 0;
		int heightEnv = 0;
		HashMap<Integer,PositionMapObject> Hash = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(in_s);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("Element");
			Hash = new HashMap<Integer,PositionMapObject>(nList.getLength());
			for (int temp = 0; temp < nList.getLength(); temp++) {
		 
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		 			Element eElement = (Element) nNode; 
					Hash.put(temp+1, new PositionMapObject(Integer.parseInt(eElement.getElementsByTagName("posx").item(0).getTextContent()), Integer.parseInt(eElement.getElementsByTagName("posy").item(0).getTextContent()), Integer.parseInt(eElement.getElementsByTagName("width").item(0).getTextContent()), Integer.parseInt(eElement.getElementsByTagName("height").item(0).getTextContent()), Integer.parseInt(eElement.getElementsByTagName("numeroquestion").item(0).getTextContent())));
		 		}
				
			}
			
			NodeList Listenv = doc.getElementsByTagName("Environnement");
			for (int tempenv = 0; tempenv < Listenv.getLength(); tempenv++) {
		 		Node nNodeenv = Listenv.item(tempenv);
				if (nNodeenv.getNodeType() == Node.ELEMENT_NODE) {
					Element eElementenv = (Element) nNodeenv;
					widthEnv = Integer.parseInt(eElementenv.getElementsByTagName("width").item(0).getTextContent());
					heightEnv = Integer.parseInt(eElementenv.getElementsByTagName("height").item(0).getTextContent());
				}
				
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		  
		
		
		return new MapEnvironnement(widthEnv, heightEnv, Hash);
	}
}